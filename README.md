# Wprowadzenie do GIT i Markdown - 15.11.2021

Szkolenie będzie prowadzone w formie interaktywnych warsztatów, dlatego żeby wziąć udział będziecie potrzebowali zainstalować kilka narzędzi:
* GIT
* Interfejs graficzny GitHub Desktop
* Konto na GitLab

Warsztaty będą podzielone na 2 części. W pierwszej omówimy podstawy Markdown oraz działania i korzystania z GITa. W drugiej przedstawione zostaną bardziej zaawansowane zagadnienia oraz nasz workflow pracy z kodem. Ta część jest skierowana głównie dla osób które zamierzają pracować w SKAR z softem.

Jeśli ktoś już umie korzystać z GITa, istnieje możliwość zwolnienia się z brania udziału w szkoleniu. Więcej informacji niżej.

## Potrzebne oprogramowanie

### GIT
Aby zacząć korzystać z GITa, trzeba go najpierw zainstalować :) Poniżej macie instrukcje:

#### Windows
1. Wejdź na [tę stronę](https://git-scm.com/) i pobierz instalator GITa.
1. Zainstaluj go na swoim komputerze w dowolnym miejscu. Wybierz **`Use Visual Studio Code as Git's default editor`**, a pozostałe opcje możesz zostawić domyślnie.
1. Sprawdź, czy GIT zainstalował się pomyślnie. Otwórz dowolną konsolę (Wiersz polecenia, PowerShell, GIT Bash) i wpisz komendę `git --version`. Jeśli pojawi się: `git version 2.33.1.windows.1` (lub dowolna wersja powyżej `2.23`), to wszystko jest okej.

#### Ubuntu
Wykonaj następujące polecenia:
1. `sudo apt update`
1. `sudo apt install git`
1. `git --version`

Jeśli pojawi się: `git version 2.33.1` (lub dowolna wersja powyżej `2.23`), to wszystko jest okej.

### GitHub Desktop
1. Instalator można pobrać [stąd](https://desktop.github.com/).
1. Nie musicie zakładać konta na GitHub (za to potrzebne będzie konto na GitLab).
1. Szczegóły korzystania z GitHub desktop będą na warsztatach.

### GitLab
1. W SKAR pracujemy z użyciem narzędzi dostarczanych przez GitLab.
1. Załóżcie konto [tutaj](https://gitlab.com/users/sign_up/).

## Dostęp do repo
W jednej z części szkolenia, będziemy pracować na wspólnym repozytorium zdalnym, do którego musicie otrzymać dostęp. Po założeniu konta GitLab, podeślijcie do mnie wasz nick lub link do konta (przez Teamsy/email/Discord). Postarajcie się to zrobić najpóźniej do początku szkolenia.

## Zwolnienie ze szkolenia
W ramach warsztatów będziemy m.in. wspólnie tworzyli własne repozytorium, wykonywali na nim podstawowe operacje, konfigurowali repo zdalne oraz wypychali/zaciągali zmiany z serwera.
Aby zwolnić się ze szkolenia wykonaj następujące zadanie:

> W gałęzi `password` tego repozytorium znajdziesz plik z hasłem. Niestety, został on przez kogoś nadpisany, więc straciliśmy jego treść. Na szczęście git przechowuje historię zmian pliku! Twoim zadaniem jest sklonowanie repo, znalezienie odpowiedniego commita we wspomnianej gałęzi oraz odzyskanie tajemnego hasła. Następnie utwórz
hash md5 z ciągu `<twój_nick>_<hasło>` (mogą być polskie znaki, np. `Hadenir_ŻyrafyDoSzafy`) i zapisz go w pliku nazwanym Twoim nickiem (np. `Hadenir.txt`). Nowy plik zacommituj i wypchnij na gałąź `tasks`.\
Możesz skorzystać ze strony [md5hashgenerator](https://www.md5hashgenerator.com/) aby wygenerować hash.\
Po wykonaniu zadania skontaktuj się ze mną przez email/Discord/Teams ;)

## Kontakt
W razie jakichkolwiek pytań/problemów piszcie do mnie:\
Konrad Brzózka\
hadenir(at)gmail.com
Hadenir#5935
